const Pool = require('pg').Pool
require('dotenv').config()

const poolKeycloak = new Pool({
    user: process.env.PGUSER2,
    host: process.env.PGHOST2,
    port: process.env.PGPORT2,
    database: process.env.PGDATABASE2,
    password: process.env.PGPASSWORD2
});

const poolFriends = new Pool({
    user: process.env.PGUSER2,
    host: process.env.PGHOST2,
    port: process.env.PGPORT2,
    database: process.env.PGDATABASE3,
    password: process.env.PGPASSWORD2
});

const updateUser = (request, response) => {    
    const idUser = request.params.idUser
    const { first_name, last_name, email } = request.body    

    poolKeycloak.query(
        `UPDATE user_entity
        SET first_name = $2,
            last_name = $3,
            email = $4,
            email_constraint = $4
        WHERE id = $1
        RETURNING id,
            first_name,
            last_name,
            email
        `, 
        [idUser, first_name, last_name, email],
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }

            response.status(200).json(results.rows ? results.rows[0] : null)
        }
    )
}

const getUser = (request, response) => {
    const idUser = request.params.idUser

    poolKeycloak.query(
        'SELECT * FROM public.user_entity WHERE id = $1', 
        [idUser],
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }

            response.status(200).json(results.rows ? results.rows[0] : null)
        }
    )
}

//GET ALL USERS
const getUsers = (request, response) => {
    const filter = request.query.filter

    const query = 'SELECT * FROM public.user_entity' + (filter !== undefined ? ` WHERE LOWER(CONCAT(first_name, ' ', last_name)) LIKE LOWER('%${filter}%')` : '')

    poolKeycloak.query(
        query, 
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }
            response.status(200).json(results.rows)
        }
    )
}

const subscribeUser = (request, response) => {
    const idUser = request.body.idUser
    const idFriend = request.body.idFriend

    poolFriends.query(
        `INSERT INTO friends VALUES (uuid_generate_v1(), $1, $2) returning id`,
        [idUser, idFriend],
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }
            
            response.status(200).json({idRelation: results.rows[0].id})
        }
    )
}

const unsubscribeUser = (request, response) => {
    const idRelation = request.body.idRelation

    poolFriends.query(
        `DELETE FROM friends WHERE id=$1`,
        [idRelation],
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }

            response.status(200).json({idRelation: idRelation})
        }
    )
}

const getFriendUser = (request, response) => {
    const idUser = request.params.idUser
    const idFriend = request.params.idFriend

    poolFriends.query(
        `SELECT id FROM friends WHERE idUser=$1 and idFriend=$2`,
        [idUser, idFriend],
        (error, results) => {
            if (error) {
                response.status(500).json(error);
                throw error
            }
            
            response.status(200).json(results.rows.length ? results.rows[0].id : null)
        }
    )
}

const getFriends = (request, response) => {
    poolFriends.query('SELECT * FROM friends', (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        
        response.status(200).json(results.rows)
    })
}

const getFriendsByIdUser = (request, response) => {
    const id = request.params.idUser

    poolFriends.query('SELECT * FROM friends WHERE idUser = $1', [id], (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        response.status(200).json(results.rows)
    })
}

module.exports = {
    getUser,
    getUsers,
    subscribeUser,
    unsubscribeUser,
    getFriendUser,
    getFriends,
    getFriendsByIdUser,
    updateUser
}