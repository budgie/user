const Pool = require('pg').Pool

const pool = new Pool();

//GET ALL FRIENDS
const getFriends = (request, response) => {
    pool.query('SELECT * FROM friends', (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        
        response.status(200).json(results.rows)
    })
}

//GET FRIENDS BY ID
const getFriendsByIdUser = (request, response) => {
    const id = request.params.idUser

    pool.query('SELECT * FROM friends WHERE idUser1 = $1 or idUser2 = $1', [id], (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        response.status(200).json(results.rows)
    })
}

//GET POST BY USER
const getPostByUser = (request, response) => {
    const user = request.params.id

    pool.query('SELECT * FROM posts WHERE user = $1', [user], (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        response.status(200).json(results.rows)
    })
}

//CREATE FRIEND
const createFriends = (request, response) => {
    const { idUser1, idUser2 } = request.body

    pool.query('INSERT INTO posts (id, idUser1, idUser2) VALUES (uuid_generate_v1(), $1, $2)', [idUser1, idUser2], (error, results) => {
        if (error) {
            response.status(500).json(error);
            throw error
        }
        response.status(201).send(`Post added with ID: ${results.insertId}`)
    })
}

module.exports = {
    getFriends,
    getFriendsByIdUser,
    createFriends
}