const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const app = require('../server.js');

chai.use(chaiHttp);

describe("Posts", () => {
    //Test get route
    describe("GET /users", () => {
            //Get all posts
            it('should get all users record', (done) => {
                chai.request(app)
                  .get('/users')
                  .end((err, res) => {
                    res.should.have.status(200);
                    done();
                  });
              });

        //Get user by id
        it("should get a single user by id", (done) => {
            const id = "2014566e-dc0e-465f-9332-e83657a1b4a5";
            chai.request(app)
            .get(`/users/${id}`)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        //Get all friends
        it("should get all friends", (done) => {
            chai.request(app)
            .get(`/friends`)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        //Get user by id
        it("should get a single user by id", (done) => {
            const id = "2014566e-dc0e-465f-9332-e83657a1b4a5";
            chai.request(app)
            .get(`/users/${id}`)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        //Get friend of a user
        it("should get a friend of a user ", (done) => {
            const idUser = "2014566e-dc0e-465f-9332-e83657a1b4a5";
            const idFriend = "cbaa948a-232b-4ac9-bb23-e1c1300e02c5";
            chai.request(app)
            .get(`/friends/${idUser}/users/${idFriend}`)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });
    });
    
    /*
    //Test post route
    describe('/POST /users', () => {
        it('should create friends relation', (done) => {
            const friend = {
                iduser : '2713aaaf-29fe-4568-8830-cf92a19f8da5',
                idfriend : '2014566e-29fe-4568-8830-e83657a1b4a5'
            }
            chai.request(app).post('/friends').send(friend).end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('id');
                res.body.should.have.property('iduser');
                res.body.should.have.property('idfriend');
                done();
            });
        })
    }) */

    describe('/DELETE/:id friend', () => {
        it('it should DELETE a relation', (done) => {
            chai.request(app)
            .delete('/friends')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });
    });
});
