const express = require('express')
const bodyParser = require('body-parser')
const { Client } = require('pg')
const { getPostByUser } = require('./friends')
const { getUser, getUsers, subscribeUser, getFriendUser, unsubscribeUser, getFriends, getFriendsByIdUser, updateUser } = require('./users')
const cors = require("cors");

require('dotenv').config()

const app = express()

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

const client = new Client()
client.connect()
    .then ((test) => {
        app.listen(process.env.PORT, process.env.IP, () => {
            console.log('Listening on ' + process.env.IP + ':' + process.env.PORT)
        });
    })
    .catch(err => {
        console.log(err)
    })


app.get('/users', (req, res) => getUsers(req, res))

app.get('/users/:idUser', (req, res) => getUser(req, res))
app.put('/users/:idUser', (req, res) => updateUser(req, res))

app.get('/friends', (req, res) => getFriends(req, res))

app.get('/friends/:idUser', (req, res) => getFriendsByIdUser(req, res))

app.post('/friends', (req, res) => subscribeUser(req, res))
app.delete('/friends', (req, res) => unsubscribeUser(req, res))

app.get('/friends/:idUser/users/:idFriend', (req, res) => getFriendUser(req, res))