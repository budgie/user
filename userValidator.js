const Joi = require('@hapi/joi');

function validateUser(user) {

    const schema = Joi.object({
        firstname: Joi.required().string(),
        lastName: Joi.required().string(),
        mail: Joi.required().mail(),
        password: Joi.required(),
        repeat_password: Joi.ref(password)
    })

    return schema.validate(user)
}

module.exports = validateUser