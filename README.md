# User

The back User manages the informations about the users.


### Prerequisites

You need to install [npm](https://www.npmjs.com/get-npm) and [nodejs](https://nodejs.org/en/download/).

### Installing

Go in the project then install dependencies

```
npm install
```

You need to put the following variables in a .env file 
```
IP=localhost
PORT=<Port>
PGUSER=<User>
PGHOST=<Host>
PGPASSWORD=<Password>
PGDATABASE=<DBName>
PGPORT=<DBPort>
PGSSLMODE=require
PGUSER2=<User2>
PGHOST2=<Host2>
PGPASSWORD2=<Password2>
PGPORT2=<DBPort2>
```

### Launch

You just have to do the following command

```
node server.js
```
